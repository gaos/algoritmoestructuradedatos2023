import random as r
import time as t

def guardado(dato, bandera):
    if bandera ==1:
        tiempo = open("taller.csv", "a")
        tiempo.write("\n"+ str(dato)+ ",")
        tiempo.close()
    elif bandera ==2:
        tiempo = open("taller.csv", "a")
        tiempo.write(str(dato)+",")
        tiempo.close()


# calcula
def calcular(matriz1, matriz2, x, y):
    contador = 0
    for i in range(len(matriz1)):
        contador = contador + matriz1[x][i] * matriz2[i][y]
    return contador

# multiplica la matriz, calculando esta
def multiplicaMatriz(matriz1, matriz2, matriz3):
    for i in range(len(matriz1)):
        for j in range(len(matriz1)):
            calculo = calcular(matriz1, matriz2, i, j)
            matriz3[i][j]= calculo


# crea la matriz del tamaño n
def creaMatriz(tamanho):
    matriz = []
#    azar = r.randint(0,10)
    for i in range(tamanho):
        matriz.append([0]*tamanho)

    return matriz

# imprime la matriz
def imprimeMatriz(matriz):
    for i in range(len(matriz)):
        for j in range(len(matriz)):
            print( "{%d}"%matriz[i][j], end="")
        print("")

# asigna al azar valores a la matriz
def asignaAzar(matriz):
    for i in range(len(matriz)):
        for j in range(len(matriz)):
            matriz[i][j]= r.randint(0,9)


# calcular o(d) y t(n) de todo

# funcion principal
def main():
    cantidad_n = 2000
    pruebas = 5

    while cantidad_n <10000:
        guardado(cantidad_n, 1)
        for i in range(pruebas):
            inicio = t.time()

            matriz = creaMatriz(cantidad_n)
            matriz2 = creaMatriz(cantidad_n)
            asignaAzar(matriz)
            #imprimeMatriz(matriz)
            #print("matriz 2")
            asignaAzar(matriz2)
            #imprimeMatriz(matriz2)
            matriz3 = creaMatriz(cantidad_n)
            multiplicaMatriz(matriz, matriz2, matriz3)
            #print("Matriz resultado")
            #imprimeMatriz(matriz3)
            fin = t.time()
            tiempo = fin - inicio
            guardado(tiempo, 2)

        cantidad_n = cantidad_n + 1000


main()
